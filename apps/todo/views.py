from django.shortcuts import get_object_or_404, redirect, render
from django.http import JsonResponse
from django.core.urlresolvers import reverse_lazy
from django.views import generic
from .models import Task
from .forms import CreateTaskForm
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required


class TaskList(generic.ListView):
    model = Task
    paginate_by = 20


class TaskUpdate(generic.UpdateView):
    model = Task
    success_url = reverse_lazy('todo:task_list')
    form_class = CreateTaskForm

    def get_form_kwargs(self):
        kwargs = super(TaskUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs


class TaskCreate(generic.CreateView):
    model = Task
    success_url = reverse_lazy('todo:task_list')
    form_class = CreateTaskForm
    template_name = 'todo/task_list.html'

    def get_form_kwargs(self):
        kwargs = super(TaskCreate, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(TaskCreate, self).get_context_data(**kwargs)
        context['task_list'] = Task.objects.all()[:20]
        return context


class TaskDelete(generic.DeleteView):
    model = Task
    success_url = reverse_lazy('todo:task_list')


@login_required()
def delete_task(request, pk):
    task = get_object_or_404(Task, pk=pk)
    if request.method == 'POST':
        if (request.user == task.user.user):
            task.delete()
        return redirect(reverse_lazy('todo:task_list'))
    return render(request, 'todo/confirm_task_delete.html', {"task": task})


@login_required()
@require_http_methods(['POST'])
def confirm_unconfirm_task(request, pk):
    data = {}
    task = None
    try:
        task = Task.objects.get(pk=pk)
        task.is_complete = not task.is_complete
        task.confirmed_by = request.user.profile
        task.save()
        status = 200
        data['message'] = 'Task confirmed successfully'
        data['confirmed_by'] = request.user.username
    except Task.DoesNotExist:
        status = 404
        data['message'] = 'Task not found'
    return JsonResponse(data, status=status)
