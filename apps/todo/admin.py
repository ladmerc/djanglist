from django.contrib import admin
from .models import Profile, Task

# Register your models here.


class TasksInLine(admin.TabularInline):
    model = Task
    extra = 1
    fk_name = "user"


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ("username", "task_count")
    search_fields = ['user__username']

    inlines = [TasksInLine]


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ("title", "user", "is_complete", "is_overdue")
