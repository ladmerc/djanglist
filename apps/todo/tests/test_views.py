import datetime
from django.test import TestCase, RequestFactory
from django.contrib.staticfiles.testing import LiveServerTestCase
from django.core.urlresolvers import (reverse)
from selenium import webdriver
from django.contrib.auth import get_user_model
from django.contrib.auth import get_user
from django.conf import settings
from apps.todo.forms import CreateTaskForm
from apps.todo.models import Task
from autofixture import AutoFixture
# from config.test_runner import GenericViewTest

# Create your tests here.


class HomeViewTest(TestCase):

    def setUp(self):
        pass

    def test_view_url_exists_at_desired_location(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('todo:task_list'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_template(self):
        resp = self.client.get(reverse('todo:task_list'))
        self.assertTemplateUsed(resp, 'todo/task_list.html')
        self.assertTemplateUsed(resp, 'base.html')

    def test_context_in_view(self):
        pass

    def test_title_correct(self):
        pass

    def test_all_urls(self):
        pass


class AuthViewTest(LiveServerTestCase):

    def setUp(self):
        self.User = get_user_model()
        pass
        # self.browser = webdriver.Firefox()
        # self.browser.implicitly_wait(3)

    def tearDown(self):
        pass
        # self.browser.quit()

    def test_login_and_signup_urls(self):
        signup_resp = self.client.get(reverse('register'), follow=True)
        login_resp = self.client.get(reverse('login'), follow=True)
        self.assertEqual(signup_resp.status_code, 200)
        self.assertEqual(login_resp.status_code, 200)

    def test_login_signup_templates(self):
        signup_resp = self.client.get(reverse('register'))
        login_resp = self.client.get(reverse('login'))
        self.assertTemplateUsed(signup_resp, 'registration/register.html')
        self.assertTemplateUsed(login_resp, 'registration/login.html')

    def __make_user(self):
        data = {"username": "test_username",
                "password1": "password", "password2": "password"}
        url = reverse('register')
        self.client.post(url, data)

    def test_signup_creates_user(self):
        self.__make_user()
        user = self.User.objects.get(username="test_username")
        self.assertIsInstance(user, self.User)

    def test_signup_authenticates_user(self):
        self.__make_user()
        user = get_user(self.client)
        assert(user.is_authenticated())


class TaskViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        #  Create 40 tasks for pagination tests
        fixture = AutoFixture(Task)
        fixture.create(40)

    def setUp(self):
        self.__make_user()
        self.rf = RequestFactory()
        User = get_user_model()
        user = User.objects.get(username="test_username")
        self.rf.user = user

    def test_create_task_views_pass_request__to_form(self):
        create_url = reverse('todo:task_new')
        response = self.client.post(create_url)
        form = response.context_data['form']
        self.assertTrue(form.request is not None)

    def test_update_task_views_pass_request__to_form(self):
        self.__make_task()
        task = Task.objects.first()
        create_url = reverse('todo:task_edit', args=(task.id,))
        response = self.client.post(create_url)
        form = response.context_data['form']
        self.assertTrue(form.request is not None)

    def test_create_task_views_context(self):
        create_url = reverse('todo:task_new')
        response = self.client.post(create_url)
        self.assertIn('task_list', response.context)

    def __make_task(self):
        today = datetime.date.today()
        form_data = {"title": 'a dummy task',
                     "due_date": today, "priority": 2}
        form = CreateTaskForm(data=form_data, request=self.rf)
        form.save()

    def __make_user(self):
        data = {"username": "test_username",
                "password1": "password", "password2": "password"}
        url = reverse('register')
        self.client.post(url, data)

    def test_delete_task_if_task_owner_valid(self):
        self.__make_task()
        task = Task.objects.first()
        url = reverse('todo:task_delete', args=(task.id,))
        response = self.client.post(url)
        self.assertEqual(response.status_code, 302)
        #  confirm task deletion
        self.assertRaises(Task.DoesNotExist)

    def test_delete_task_template(self):
        self.__make_task()
        task = Task.objects.first()
        url = reverse('todo:task_delete', args=(task.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'todo/confirm_task_delete.html')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'header.html')

    def test_confirm_unconfirm_task(self):
        self.__make_task()
        task = Task.objects.first()
        url = reverse('todo:task_confirm', args=(task.id,))
        self.client.post(url)
        saved_task = Task.objects.first()
        self.assertTrue(saved_task.is_complete)
        #  post again to unmark
        self.client.post(url)
        self.assertFalse(Task.objects.first().is_complete)
        self.assertEqual(saved_task.confirmed_by, self.rf.user.profile)

    def test_task_confirm_response(self):
        self.__make_task()
        task = Task.objects.first()
        url = reverse('todo:task_confirm', args=(task.id,))
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
        self.assertIn('confirmed successfully', str(response.content))

        #  test invalid id
        invalid_url = reverse('todo:task_confirm', args=(100,))
        response = self.client.post(invalid_url)
        self.assertEqual(response.status_code, 404)
        self.assertIn('Task not found', str(response.content))

    def test_is_paginated_by_twenty(self):
        response = self.client.get(reverse('todo:task_list'))
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'] is True)
        self.assertTrue(len(response.context['object_list']) == 20)

    # def test_redirect_if_not_logged_in(self):
    #     response = self.client.get(reverse('todo:home'))
    #     self.assertRedirects(response, '/accounts/login/?next=/')

    # def test_redirect_url()
    # def test_only_staff_edit_other_profile()
