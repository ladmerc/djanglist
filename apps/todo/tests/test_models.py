import datetime, random
from django.test import TestCase
from apps.todo.models import Profile, Task
from django.contrib.auth import get_user_model
from autofixture import AutoFixture

User = get_user_model()


class ProfileModelTest(TestCase):

    user = None

    @classmethod
    def setUpClass(cls):
        cls.user = User.objects.create(
            username="ladmerc", password="password",

            first_name="ladna", last_name="meke")
        super(ProfileModelTest, cls).setUpClass()

    def setUp(self):
        self.fields = {field.name: field for field in
                       Profile._meta.fields}
        self.test_fields = ['email_confirmed']

    def test_verbose_name_plural(self):
        self.assertEqual(str(Profile._meta.verbose_name_plural), "Profiles")

    def test_all_model_fields_exist(self):
        """test that the expected fields (test_fields) all
        exist in the model fields
        """
        for field in self.test_fields:
            self.assertTrue(field in self.fields)

    def test_profile_creation(self):
        self.assertIsInstance(self.user.profile, Profile)
        self.user.save()

    def test_model_field_labels_correct(self):
        for field in self.test_fields:
            field_label = Profile._meta.get_field(field)
            self.assertTrue(field_label)

    def test_extra_profile_properties(self):
        self.assertIsNotNone(self.user.profile.username)

    def test_tasks_count(self):
        fixture1 = AutoFixture(User)
        fixture1.create(10)
        fixture2 = AutoFixture(Task)
        tasks = fixture2.create(10)
        rand_task = tasks[random.randint(0, 9)]  # task user needed
        user_tasks = Task.objects.filter(user=rand_task.user)
        self.assertEqual(rand_task.user.task_count, len(user_tasks))

    def test_string_representation(self):
        expected_object_name = "{} {}".format(
            self.user.first_name, self.user.last_name)
        self.assertEqual(expected_object_name, str(self.user.profile))


class TaskModelTest(TestCase):

    def setUp(self):
        today = datetime.date.today()
        last_week = today - datetime.timedelta(weeks=1)
        self.task1 = Task(title="Undue task", priority=2, due_date=today)
        self.task2 = Task(title="Due Task", priority=3, due_date=last_week)

    def test_string_representation(self):
        self.assertEqual(self.task1.title, str(self.task1))

    def test_title_max_length(self):
        max_length = self.task1._meta.get_field('title').max_length
        self.assertEqual(max_length, 50)

    def test_is_overdue_task(self):
        self.assertFalse(self.task1.is_overdue)
        self.assertTrue(self.task2.is_overdue)
