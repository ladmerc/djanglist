import datetime
from django.core.urlresolvers import reverse
from django.test import TestCase, RequestFactory
from apps.todo.forms import CreateTaskForm
from django.contrib.auth import get_user_model
from apps.todo.models import Task


class SignUpFormTest(TestCase):
    pass


class TaskFormTest(TestCase):

    def setUp(self):
        self.__make_user()
        self.rf = RequestFactory()
        User = get_user_model()
        user = User.objects.get(username="test_username")
        self.rf.user = user

    def __make_user(self):
        data = {"username": "test_username",
                "password1": "password", "password2": "password"}
        url = reverse('register')
        self.client.post(url, data)

    def test_invalid_due_date(self):
        today = datetime.date.today()
        yday = today - datetime.timedelta(days=1)
        form_data = {"title": 'a dummy task',
                     "due_date": yday, "priority": 2}
        form = CreateTaskForm(data=form_data, request=self.rf)
        self.assertFalse(form.is_valid())

    def test_valid_due_date(self):
        today = datetime.date.today()
        form_data = {"title": 'a dummy task',
                     "due_date": today, "priority": 2}
        form = CreateTaskForm(data=form_data, request=self.rf)
        self.assertTrue(form.is_valid())

    def test_unique_user_title(self):
        form_data1 = {"title": 'a dummy task', "priority": 2}
        form_data2 = {"title": 'a dummy task', "priority": 1}
        form1 = CreateTaskForm(data=form_data1, request=self.rf)
        self.assertTrue(form1.is_valid())
        form1.save()
        form2 = CreateTaskForm(data=form_data2, request=self.rf)
        self.assertFalse(form2.is_valid())

    def test_form_saves_profile(self):
        # TODO: test why not working without priority
        form_data = {"title": 'a dummy task',
                     "due_date": datetime.date.today(), "priority": 2}
        form = CreateTaskForm(data=form_data, request=self.rf)
        form.save()
        task = Task.objects.first()
        self.assertEqual(task.user.user, self.rf.user)
