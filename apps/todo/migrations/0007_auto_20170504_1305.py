# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0006_auto_20170501_2239'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='task',
            options={'verbose_name': 'Task', 'verbose_name_plural': 'Tasks', 'ordering': ('-created_at', 'title')},
        ),
        migrations.AddField(
            model_name='task',
            name='description',
            field=models.TextField(default='unavailable', help_text='task description'),
            preserve_default=False,
        ),
    ]
