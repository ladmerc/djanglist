# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import apps.todo.managers


class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('title', models.CharField(max_length=100)),
                ('priority', models.IntegerField(default=1, choices=[(1, 'Low'), (2, 'Medium'), (3, 'High')])),
                ('is_complete', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('due_date', models.DateField()),
                ('completed_date', models.DateField()),
                ('user', models.ForeignKey(verbose_name='tasks', related_name='tasks', to='todo.Profile')),
            ],
            options={
                'verbose_name': 'Task',
                'verbose_name_plural': 'Tasks',
                'ordering': ('user', 'title'),
            },
            managers=[
                ('objects', apps.todo.managers.ProjectManager()),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='task',
            unique_together=set([('user', 'title')]),
        ),
    ]
