# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0005_auto_20170501_1310'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='confirmed_by',
            field=models.ForeignKey(verbose_name='Approval', null=True, related_name='confirms', to='todo.Profile'),
        ),
        migrations.AlterField(
            model_name='task',
            name='due_date',
            field=models.DateField(blank=True, null=True, help_text='intended date of completion'),
        ),
        migrations.AlterField(
            model_name='task',
            name='title',
            field=models.CharField(max_length=50, help_text='Name of this task'),
        ),
        migrations.AlterField(
            model_name='task',
            name='user',
            field=models.ForeignKey(verbose_name='User', related_name='tasks', to='todo.Profile'),
        ),
    ]
