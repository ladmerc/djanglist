(function(global) {
    'use strict';
    
    angular.module('app')
    .service('AppService', function($http) {
        
        var serviceObj = {
            createTask: function(data) {
                return $http.post('/task/create/', data)
            }
        }

        return serviceObj;

    });
    
})(window)