(function(global) {
    'use strict';
    
    angular.module('app')
    .controller('HomeCtrl', function($scope, AppService) {
        
        function resetForm() {
            $scope.task = {
                priority: '2',
                title: ''
            }
            
        }

        $scope.createTask = function() {
            console.log($scope.task)
            $scope.task.priority = parseInt($scope.task.priority)
            AppService.createTask($scope.task)
            .then(function(response) {
                console.log(response);
                resetForm();
            }).catch(function err(error) {
                console.log(error)
            })
        }
    });
    
})(window)