(function(global){
    'use strict';
    
    angular.module('app', [])

    // .config(function($httpProvider, $interpolateProvider) {
    //     $interpolateProvider.startSymbol('[{');
    //     $interpolateProvider.endSymbol('}]');
    //     var csrf = document.querySelector('input[name=csrfmiddlewaretoken]')
    //     console.log(csrf)
    //     $httpProvider.defaults.headers.post['X-CSRFToken'] = csrf.value;
    // });

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip(); 
        $('.toggle-complete').click(toggleCompleteHandler)
        $('.task-confirm').click(confirmHandler)
        $('.float-button').click(toggleFormHandler)

        function toggleCompleteHandler() {
            var lists = $('.bt-complete').parents('li')
            if (lists.is(':visible')) {
                lists.hide()
                $(this).text('Show completed tasks')
            } else {
                lists.show()
                $(this).text('Hide completed tasks')
            }
        }

        function toggleFormHandler() {
            var form = $('.add-todo');
            $(this).find('.glyphicon').toggleClass('glyphicon-chevron-up')
            if (form.is(':visible')) {
                form.hide()
                $(this).attr('data-original-title', 'add new task')
            } else {
                form.show()
                $(this).attr('data-original-title', 'hide form')
            }
        }

        function confirmHandler() {
            // loading
            var btn = $(this);
            if (btn.parent('button').hasClass('bt-complete')) {
                return false;
            }
            var spinner = btn.siblings('img')
            spinner.show()

            var taskId = btn.attr('data-task-id')
            var url = btn.attr('data-url')
            var token = $('input[name="csrfmiddlewaretoken"').val()

            var data = {
                csrfmiddlewaretoken: token,
                pkspp: taskId
            }
            $.post(url, data)
            .then(function(response) {
                console.log(response)
                var parent = btn.parent('button');
                parent.addClass('bt-complete')
                parent.attr('title', 'Confirmed by ' + response.confirmed_by)
                parent.attr('data-toggle','tooltip')
                parent.tooltip();
            })
            .catch(function(e) {
                console.log(e)
            })
            .always(function() {
                spinner.hide()
            })
        }

    })


})(window)
