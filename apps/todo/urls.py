from django.conf.urls import url
from .import views
from django.contrib.auth.decorators import login_required


urlpatterns = [
    url(r'^$', login_required(views.TaskList.as_view()), name="task_list"),
    url(r'^task/new$', login_required(
        views.TaskCreate.as_view()), name='task_new'),
    url(r'^task/(?P<pk>\d+)/edit/$',
        login_required(views.TaskUpdate.as_view()), name="task_edit"),
    url(r'^task/(?P<pk>\d+)/delete/$',
        views.delete_task, name="task_delete"),
    url(r'^task/(?P<pk>\d+)/confirm/$',
        views.confirm_unconfirm_task, name="task_confirm"),
]
