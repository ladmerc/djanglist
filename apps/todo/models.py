from datetime import date
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from . import managers

# Create your models here.


class Profile(models.Model):

    # Relations
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        related_name='profile',
        verbose_name='user',
        on_delete=models.CASCADE
    )

    # Attributes - Mandatory
    email_confirmed = models.BooleanField(default=False)
    # Attributes - Optional
    # Object Manager
    objects = managers.ProfileManager()

    # Custom Properties
    @property
    def task_count(self):
        return self.tasks.count()

    @property
    def username(self):
        # able to do 'profile.username' as opposed to 'profile.user.username'
        return self.user.username

    # Methods

    # Meta and String
    class Meta:
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"

    def __str__(self):
        return '{} {}'.format(self.user.first_name, self.user.last_name)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_user_profile(sender, instance, created, **kwargs):
    #  automatically create profile whenever a new user is created
    if created:
        profile = Profile(user=instance)
        profile.save()


class Task(models.Model):

    # Relations
    user = models.ForeignKey(
        Profile,
        related_name='tasks',
        verbose_name='User'
    )

    PRIORITY = (
        (1, 'Low'),
        (2, 'Medium'),
        (3, 'High'),
    )

    # Attributes - Mandatory
    title = models.CharField(max_length=50, help_text="Name of this task")
    # description = models.TextField(help_text="task description")
    priority = models.IntegerField(choices=PRIORITY, default=1)
    is_complete = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    confirmed_by = models.ForeignKey(
        Profile,
        related_name='confirms',
        verbose_name='Approval',
        null=True
    )

    # Attributes - Optional
    due_date = models.DateField(
        null=True, blank=True, help_text="intended date of completion")
    completed_date = models.DateField(null=True, blank=True)

    # Object Manager
    objects = managers.ProjectManager()

    # Custom Properties
    @property
    def is_overdue(self):
        if self.due_date and date.today() > self.due_date:
            return True
        return False

    # Methods

    # Meta and String
    class Meta:
        verbose_name = "Task"
        verbose_name_plural = "Tasks"
        ordering = ("-created_at", "title")
        unique_together = ("user", "title")

    def __str__(self):
        return self.title
