import datetime
from django import forms
from .models import (Task)
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError


class SignUpForm(forms.ModelForm):

    class Meta:
        model = get_user_model()
        fields = ('username', 'password')


class CreateTaskForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(CreateTaskForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Task
        fields = ('title', 'priority', 'due_date', 'is_complete')

    def clean_due_date(self):
        if self.cleaned_data['due_date']:
            field = self.cleaned_data['due_date']
            if field < datetime.date.today():
                raise ValidationError('Invalid date - due date in past')
            return field

    def clean_title(self):
        data = self.cleaned_data['title']
        title_occurrence = Task.objects.filter(
            title__iexact=data.lower(), user=self.request.user.profile).count()
        is_create = self.instance.pk is None  # dont validate on update
        if is_create and title_occurrence > 0:
            raise ValidationError('You already have a task with this name')
        return data

    def save(self, commit=False):
        self.instance.user = self.request.user.profile
        self.instance.save()
        return self.instance
