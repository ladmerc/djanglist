#!/usr/bin/env python
import os
import sys
import environ


if __name__ == "__main__":
    app_settings = environ.Env()('DJANGO_SETTINGS_MODULE',
                                 default="config.settings.prod")
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", app_settings)

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
