from unittest import skip
from django.test import TestCase
from django.contrib.staticfiles.testing import LiveServerTestCase
from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
import os
import django
import sys
from django.core.urlresolvers import (resolve, reverse)
from colorama import init as color_init
from django.conf import settings

color_init(autoreset=True)


class SettingsTest(TestCase):
    """
        Test python version, django version, and secret keys
    """

    @classmethod
    def setUpClass(cls):
        # importError handled in custom TEST_RUNNER class
        cls.settings_module = os.environ.get('DJANGO_SETTINGS_MODULE')
        cls.base = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        super(SettingsTest, cls).setUpClass()

    def setUp(self):
        pass
        # print(SettingsTest.settings_module)

    def test_python_version3(self):
        self.assertTrue(sys.version_info >= (2, 9))

    def test_django_version(self):
        self.assertIn('1.8', django.get_version())

    def test_security_key(self):
        key = settings.SECRET_KEY
        self.assertTrue(type(key) is str and len(key) > 10)


class TestMainViews(LiveServerTestCase):

    def setUp(self):
        # self.browser = webdriver.Firefox()
        # self.browser.implicitly_wait(3)
        pass

    def tearDown(self):
        # self.browser.quit()
        pass

    def get_full_url(self, namespace):
        return self.live_server_url + reverse(namespace)

    def test_home_url(self):
        # self.browser.get(self.get_full_url('todo:home'))
        resolver = resolve('/')
        self.assertEqual(resolver.url_name, 'task_list')
        self.assertEqual(resolver.view_name, 'todo:task_list')

    def test_login_redirect_url(self):
        self.assertTrue(settings.LOGIN_REDIRECT_URL == '/')

    def test_404_url(self):
        resp = self.client.get('/error_url')
        self.assertEqual(resp.status_code, 404)

    def test_404_template(self):
        resp = self.client.get('/error_url')
        self.assertTrue("page you were trying to view" in str(resp.content))
        # self.assertTemplateUsed('404.html')

    def test_robots_txt(self):
        resp = self.client.get(self.live_server_url + '/robots.txt')
        self.assertEqual(resp.status_code, 200)

    def test_humans_txt(self):
        resp = self.client.get(self.live_server_url + '/humans.txt')
        self.assertEqual(resp.status_code, 200)

    def test_favicon(self):
        resp = self.client.get('/')
        self.assertIn('favicon.ico', str(resp.content))
