from .base import *

ALLOWED_HOSTS.extend(["*"])

INSTALLED_APPS += ('autofixture',)
