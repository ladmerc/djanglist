import os
from django.test.runner import DiscoverRunner
from django.test import TestCase
from colorama import Fore


class DjangListRunner(DiscoverRunner):

    def setup_test_environment(self, **kwargs):
        settings_module = os.environ.get('DJANGO_SETTINGS_MODULE', False)
        if not settings_module:
            raise Exception("Cannot find DJANGO_SETTINGS_MODULE")

        # check if we're in test environment
        if ".test" not in settings_module:
            print(Fore.YELLOW, "Doesn't look like test env is activated")
            print(Fore.RESET)  # reset color
        super(DjangListRunner, self).setup_test_environment(**kwargs)


# class GenericModelTest(TestCase):

#     def __init__(self, *args, **kwargs):
#         if not all(prop in kwargs for prop in ("model", "test_fields", "vb_name", "str_rep")):
#             raise KeyError("Keys: 'model', 'test_fields','vb_name' and 'str_rep' not found ")
#         self.model, self.test_fields, self.vb_name, \
#             self.str_rep = kwargs['model'], kwargs['test_fields'], \
#             kwargs['vb_name'], kwargs['str_rep']
#         super(GenericModelTest, self).__init__(*args, **kwargs)

#     def setUp(self):
#         self.fields = {field.name: field for field in
#                        self.model._meta.fields}

#     def test_string_representation(self):
#         expected_object_name = "{} {}".format(
#             self.user.first_name, self.user.last_name)
#         self.assertEquals(expected_object_name, str(self.user.profile))

#     def test_model_field_labels_correct(self):
#         for field in self.test_fields:
#             field_label = self.user.profile._meta.get_field(field)
#             self.assertTrue(field_label)

#     def test_extra_profile_properties(self):
#         pass
