from setuptools import setup

setup(
    name='djanglist',
    version='1.0',
    description='Django Todo App',
    author='Meke Ladna',
    author_email='ladna_mekelive@yahoo.com',
    url='http://djanglist-meke.rhcloud.com/',
)
